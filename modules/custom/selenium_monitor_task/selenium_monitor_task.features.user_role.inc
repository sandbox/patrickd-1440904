<?php
/**
 * @file
 * selenium_monitor_task.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function selenium_monitor_task_user_default_roles() {
  $roles = array();

  // Exported role: manager
  $roles['manager'] = array(
    'name' => 'manager',
    'weight' => '2',
  );

  return $roles;
}

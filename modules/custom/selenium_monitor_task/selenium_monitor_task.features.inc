<?php
/**
 * @file
 * selenium_monitor_task.features.inc
 */

/**
 * Implements hook_views_api().
 */
function selenium_monitor_task_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_node_info().
 */
function selenium_monitor_task_node_info() {
  $items = array(
    'monitoring_task' => array(
      'name' => t('Monitoring task'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Website name'),
      'help' => '',
    ),
  );
  return $items;
}

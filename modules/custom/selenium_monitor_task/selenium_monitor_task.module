<?php
/**
 * @file
 * Code for the Selenium Monitor Task feature.
 */

include_once('selenium_monitor_task.features.inc');

/**
 * Implements hook_menu().
 */
function selenium_monitor_menu() {
  $items = array();
  // Path for updating the status of an existing task.
  $items['selenium_monitor_task/%node/%'] = array(
    'title' => 'De/activate',
    'page callback' => 'drupal_get_form', 
    'page arguments' => array('selenium_monitor_task_update_status', 1, 2),
    'access callback' => 'node_access',
    'access arguments' => array('update', 1),
  );
  return $items;
}

/**
 * Confirmation form for setting a new status of a monitoring task.
 */
function selenium_monitor_task_update_status($form, &$form_state, $node, $status) {
  $form['#node'] = $node;
  $form['#status'] = $status;

  return confirm_form(
    $form,
    t('Are you sure you want to !status %title task?', array(
      '!status' => ($status == 1) ? t('activate') : t('deactivate'),
      '%title' => $node->title,
    )),
    NULL,
    '',
    ($status == 1) ? t('Activate') : t('Deactivate')
  );
}

/**
 * Sets a new status of a monitoring task.
 */
function selenium_monitor_task_update_status_submit($form, &$form_state) {
  $node = $form['#node'];
  $status = $form['#status'];
  // Update task.
  if ($node->type == 'monitoring_task') {
    $node->field_active[$node->language][0]['value'] = (int) $status;
    node_save($node);
  }
  // Back to front.
  drupal_goto();
}

/**
 * Implements hook_node_update().
 */
function selenium_monitor_task_node_update($node) {
  selenium_monitor_task_manage($node);
}

/**
 * Implements hook_node_insert().
 */
function selenium_monitor_task_node_insert($node) {
  selenium_monitor_task_manage($node);
}

/**
 * This will generate and update selenium testsuites depending on
 * the attached testcase files of the monitoring task.
 */
function selenium_monitor_task_manage($node) {
  if ($node->type == 'monitoring_task') {

    $testcases = array();

    // Active task -> generate testsuite file.
    if ($node->field_active[$node->language][0]['value']) {
      foreach ($node->field_selenium_tests[$node->language] as $file) {
        // Get file object.
        $file_obj = file_load($file['fid']);
        // Get path.
        $wrapper = file_stream_wrapper_get_instance_by_uri($file_obj->uri);
        //$file_path = $wrapper->realpath();
        $file_path = $wrapper->getExternalUrl();
        // Add it to list.
        $testcases[] = array(
          'file_path' => $file_path,
          'file_description' => $file['description'],
          'file_name' => $file_obj->filename,
        );
      }
      // Generate testsuite file.
      $testsuite_data = selenium_monitor_task_testsuite(
        $node->field_url[$node->language][0]['url'],
        $node->title,
        $testcases
      );
      // Save it.
      $testsuite_file = (object) array(
        'filename' => 'testsuite-' . $node->nid . '.html',
        'uri' => 'public://selenium_tasks/testsuite-' . $node->nid . '.html',
        'filemime' => 'text/html',
        'status' => 1,
      );
      file_put_contents($testsuite_file->uri, $testsuite_data);
    }

    // Inactive task -> try to delete testsuite file.
    else {
      if (file_exists('public://selenium_tasks/testsuite-' . $node->nid . '.html')) {
        drupal_unlink('public://selenium_tasks/testsuite-' . $node->nid . '.html');
      }
    }
  }
}

/**
 * Implements hook_node_delete().
 *
 * Deactivate task on node deletion.
 */
function selenium_monitor_task_node_delete($node) {
  if ($node->type == 'monitoring_task') {
    if (file_exists('public://selenium_tasks/testsuite-' . $node->nid . '.html')) {
      drupal_unlink('public://selenium_tasks/testsuite-' . $node->nid . '.html');
    }
  }
}

/**
 * Generates the raw content of a selenium testuite file (HTML).
 */
function selenium_monitor_task_testsuite($url, $testsuite, $testcases) {
  $output = array();
  $output[] = '<!--' . $url . '-->';
  $output[] = '<html>';
  $output[] = '<head>';
  $output[] = '  <meta content="text/html; charset=ISO-8859-1"';
  $output[] = '    http-equiv="content-type">';
  $output[] = '  <title>' . check_plain($testsuite) . '</title>';
  $output[] = '</head>';
  $output[] = '<body>';
  $output[] = '  <table cellpadding="1" cellspacing="1" border="1">';
  $output[] = '    <tbody>';
  $output[] = '      <tr><td><b>' . check_plain($testsuite) . '</b></td></tr>';
  foreach ($testcases as $i => $testcase) {
    $output[] = '      <tr><td><a href="' . $testcase['file_path'] . '">' . 
      (empty($testcase['file_description'])
      ? check_plain($testcase['file_name'])
      : check_plain($testcase['file_description']))
      . '</a></td></tr>';
  }
  $output[] = '    </tbody>';
  $output[] = '  </table>';
  $output[] = '</body>';
  $output[] = '</html>';
  return implode("\n", $output);
}

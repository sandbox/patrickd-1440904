<?php
/**
 * @file
 * selenium_monitor_task.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function selenium_monitor_task_user_default_permissions() {
  $permissions = array();

  // Exported permission: access content
  $permissions['access content'] = array(
    'name' => 'access content',
    'roles' => array(
      0 => 'manager',
    ),
    'module' => 'node',
  );

  // Exported permission: create monitoring_task content
  $permissions['create monitoring_task content'] = array(
    'name' => 'create monitoring_task content',
    'roles' => array(
      0 => 'manager',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any monitoring_task content
  $permissions['delete any monitoring_task content'] = array(
    'name' => 'delete any monitoring_task content',
    'roles' => array(
      0 => 'manager',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own monitoring_task content
  $permissions['delete own monitoring_task content'] = array(
    'name' => 'delete own monitoring_task content',
    'roles' => array(
      0 => 'manager',
    ),
    'module' => 'node',
  );

  // Exported permission: delete revisions
  $permissions['delete revisions'] = array(
    'name' => 'delete revisions',
    'roles' => array(),
  );

  // Exported permission: edit any monitoring_task content
  $permissions['edit any monitoring_task content'] = array(
    'name' => 'edit any monitoring_task content',
    'roles' => array(
      0 => 'manager',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own monitoring_task content
  $permissions['edit own monitoring_task content'] = array(
    'name' => 'edit own monitoring_task content',
    'roles' => array(
      0 => 'manager',
    ),
    'module' => 'node',
  );

  // Exported permission: revert revisions
  $permissions['revert revisions'] = array(
    'name' => 'revert revisions',
    'roles' => array(),
  );

  // Exported permission: view own unpublished content
  $permissions['view own unpublished content'] = array(
    'name' => 'view own unpublished content',
    'roles' => array(
      0 => 'manager',
    ),
    'module' => 'node',
  );

  // Exported permission: view revisions
  $permissions['view revisions'] = array(
    'name' => 'view revisions',
    'roles' => array(
      0 => 'manager',
    ),
    'module' => 'node',
  );

  return $permissions;
}

<?php
/**
 * @file
 * selenium_monitor_result.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function selenium_monitor_result_filter_default_formats() {
  $formats = array();

  // Exported format: Raw HTML
  $formats['raw_html'] = array(
    'format' => 'raw_html',
    'name' => 'Raw HTML',
    'cache' => '1',
    'status' => '1',
    'weight' => '0',
    'filters' => array(),
  );

  return $formats;
}

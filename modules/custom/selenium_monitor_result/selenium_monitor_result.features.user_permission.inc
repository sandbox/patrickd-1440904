<?php
/**
 * @file
 * selenium_monitor_result.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function selenium_monitor_result_user_default_permissions() {
  $permissions = array();

  // Exported permission: create test_results content
  $permissions['create test_results content'] = array(
    'name' => 'create test_results content',
    'roles' => array(),
  );

  // Exported permission: delete any test_results content
  $permissions['delete any test_results content'] = array(
    'name' => 'delete any test_results content',
    'roles' => array(),
  );

  // Exported permission: delete own test_results content
  $permissions['delete own test_results content'] = array(
    'name' => 'delete own test_results content',
    'roles' => array(),
  );

  // Exported permission: edit any test_results content
  $permissions['edit any test_results content'] = array(
    'name' => 'edit any test_results content',
    'roles' => array(),
  );

  // Exported permission: edit own test_results content
  $permissions['edit own test_results content'] = array(
    'name' => 'edit own test_results content',
    'roles' => array(),
  );

  return $permissions;
}

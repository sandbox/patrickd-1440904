<?php
/**
 * @file
 * selenium_monitor_result.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function selenium_monitor_result_default_rules_configuration() {
  $items = array();
  $items['rules_send_a_mail_alert_if_a_test_failed'] = entity_import('rules_config', '{ "rules_send_a_mail_alert_if_a_test_failed" : {
      "LABEL" : "Send a mail alert if a test failed",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "test_results" : "test_results" } }
          }
        },
        { "NOT data_is" : { "data" : [ "node:field-test-pass" ], "value" : 1 } }
      ],
      "DO" : [
        { "mail" : {
            "to" : [ "node:author:mail" ],
            "subject" : "[node:title] - Selenium monitoring test failed",
            "message" : "A selenium test failed:\\r\\nTime: [node:created]\\r\\nWebsite: [node:title]\\r\\n\\r\\nResults: [node:url]\\r\\n",
            "from" : [ "site:mail" ]
          }
        }
      ]
    }
  }');
  return $items;
}

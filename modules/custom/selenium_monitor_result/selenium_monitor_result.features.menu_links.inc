<?php
/**
 * @file
 * selenium_monitor_result.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function selenium_monitor_result_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu:test-results
  $menu_links['main-menu:test-results'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'test-results',
    'router_path' => 'test-results',
    'link_title' => 'Test results',
    'options' => array(),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Test results');


  return $menu_links;
}

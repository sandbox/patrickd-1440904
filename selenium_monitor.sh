#!/bin/bash

# This script needs some more work! ;-)

# Deamonize (start this script again, but dedicated).
if [ "x$1" != "x--" ]; then
  $0 -- 1> /dev/null 2> /dev/null &
  exit 0
fi

# Path to drupal files (sites/default/files).
FILES="../../sites/default/files"

# Browser used for testing.
BROWSER="*firefox"
# Screen resolution for testing.
SCREEN_RES="1024x768x24"

# Path to selenium server .jar.
SELENIUM_SERVER="libraries/selenium_server/selenium-server.jar"

# Tasks directory.
TASKS_DIR="selenium_tasks"
# Reports directory.
REPORTS_DIR="selenium_reports"

# Switch working dir to the script's path.
CWD="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$CWD"

# Create a display job to run tests in.
Xvfb :1 -screen 0 "$SCREEN_RES" &

# Iterate through tasks.
for TASK_FILE in $FILES/$TASKS_DIR/*
do
  # Generate result file name.
  RESULT_FILE=$(echo $(basename $TASK_FILE) | sed 's/testsuite/result/')

  # Extract URL.
  read -r URL < $TASK_FILE
  URL=$(echo $URL | sed 's/<!--//' | sed 's/-->//')

  # Start the selenium server in the new display (for each task).
  DISPLAY=:1 java -jar "$SELENIUM_SERVER" -htmlSuite "$BROWSER" "$URL" "$TASK_FILE" "$FILES/$REPORTS_DIR/$RESULT_FILE"
done;

# We're done, kill the display job.
kill %1

# Let's run cron, so all reports will be inserted.
drush cron

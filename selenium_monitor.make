core=7.x

projects[views][subdir] = contrib
projects[features][subdir] = contrib
projects[ctools][subdir] = contrib
projects[link][subdir] = contrib
projects[entity_token][subdir] = contrib
projects[rules][subdir] = contrib

libraries[selenium_server][download][type] = "get"
libraries[selenium_server][download][url] = "http://selenium.googlecode.com/files/selenium-server-standalone-2.18.0.jar"
libraries[selenium_server][destination] = "libraries"
